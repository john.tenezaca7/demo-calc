import React, { FC } from "react";
import { useAppSelector } from "../../store/hooks";
import { StyledDisplayInfoSection } from "./StyledDisplayInfoSection";

interface IDisplayInfoSection {}

const DisplayInfoSection: FC<IDisplayInfoSection> = () => {
  const { equation, result, errorState } = useAppSelector(
    ({ calculatorSlice }) => calculatorSlice
  );
  return (
    <StyledDisplayInfoSection>
      <div className="calculation-section">
        <p>{equation.length === 1 && equation === "0" ? "0" : equation}</p>
      </div>
      <div className="results-section">
        <p>{result.length === 1 && result === "0" ? "0" : result}</p>
      </div>
        {errorState &&  errorState.hasError  && (
          <div className="error-section">
            <p> { errorState.msg }</p>
          </div>
        )}
      <div className="border" />
    </StyledDisplayInfoSection>
  );
};
export { DisplayInfoSection };
