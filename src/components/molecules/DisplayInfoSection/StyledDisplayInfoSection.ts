import styled from "styled-components";
import { theme } from "../../shared";

interface IStyledDisplayInfoSection {}

const {
  colors: { primary_green },
} = theme;

const StyledDisplayInfoSection = styled.div<IStyledDisplayInfoSection>`
  position: relative;
  padding: 2rem;

  p {
    margin: 0;
    text-align: right;
  }
  
  .calculation-section {
    margin: 1rem 0 0;
    p {
      color: white;
      font-size: 22px;
    }
  }

  .results-section {
    margin-bottom: 2rem;
    p {
      font-size: 80px;
      font-weight: bold;
      color: ${primary_green};
    }
  }

  .error-section {
    position: absolute;
    height: 2rem;
    right: 2rem;
    bottom: 3.5rem;

    p {
      font-weight: bold;
      color: red;
      margin: 0;
      font-size: 25px;
      letter-spacing: 3px;
    }
  }

`;

export { StyledDisplayInfoSection };
