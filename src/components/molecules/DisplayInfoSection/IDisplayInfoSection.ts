interface IDisplayInfoSection {
  calculation: string;
  results: string;
}

export type { IDisplayInfoSection };