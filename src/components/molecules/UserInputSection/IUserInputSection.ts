type ToggleBackground = boolean;
type Row_Type = IGrid_Item[];

interface IGrid_Item {
  toggle: ToggleBackground;
  value: string | null | undefined;
} 

interface IGrid {
  grid: Row_Type[];
}

interface IRow {
  row: Row_Type;
}

export type { IGrid_Item, IGrid, Row_Type, IRow };
