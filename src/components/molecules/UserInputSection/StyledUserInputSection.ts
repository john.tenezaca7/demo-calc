import styled from "styled-components";
import { theme } from "../../shared";

interface IStyledUserInputSection {}

const {
  colors: { primary_dark },
} = theme;

const StyledUserInputSection = styled.div<IStyledUserInputSection>`  
	padding: 1rem 2rem 2rem; 

  * {
    font-size: 25px;
    color: white;
  }

  .row {
    display: flex;

    .row:last-child {
      margin-right: 0;
    }

    .row-item {
      cursor: pointer;
      display: flex;
      justify-content: center;
      align-items: center;
      margin: auto;
      height: 50px;
      width: 60px;
      background-color: #444444;
      border-radius: 10px;
      margin-bottom: 2.5rem;
      transition: all ease-in-out 0.1s;

      &:hover {
        background-color: ${primary_dark};
      }
    }

    .dark-background {
      background-color: ${primary_dark};
      transition: all ease-in-out 0.1s;
      &:hover {
        background-color: #444444;
      }
    }

    .no-background {
      cursor: auto;
      background-color: ${primary_dark};

      &:hover {
        background-color: ${primary_dark};
      }
    }

    .row-item:last-child {
      margin-right: 0;
    }
  }
`;

export { StyledUserInputSection };
