interface IActiveCalac {
  value_1: string,
  value_2: string,
}

export type { IActiveCalac }; 

export { GetValues } from "./GetValues"

export * from "./math"