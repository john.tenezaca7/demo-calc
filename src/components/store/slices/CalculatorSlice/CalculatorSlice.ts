import { createSlice } from "@reduxjs/toolkit";
import { initialState, name } from "./CalculatorSliceState";
import { IEquationPayload, IResultPayload, IErrorPayload } from "./ICalculatorSlice";

const CalculatorSlice = createSlice({
  initialState,
  name,
  reducers: {
    updateResultsValue: (state, { payload: { value } }: IResultPayload) => {
      state.result = value;
    } ,
    updateEquationValue: (state, { payload: { value } }: IEquationPayload) => {
      let prev = state.equation;

      prev = prev.length === 1 && prev === "0" ? '' : prev;
      
      state.equation = prev + value;
    },
    allClear: (state) => {
      state.equation = '0';
      state.result = '0';
    },
    updateError: (state, { payload: { hasError , msg } }: IErrorPayload) => {
      state.errorState = {
        hasError, msg
      }
    }
  },
});

// Action creators are generated for each case reducer function
const CalculatorSliceActions = CalculatorSlice.actions;
export { CalculatorSliceActions };

export default CalculatorSlice.reducer;
