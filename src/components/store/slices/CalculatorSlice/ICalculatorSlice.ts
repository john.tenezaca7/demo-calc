import { PayloadAction } from "@reduxjs/toolkit";
import { ICalcGridStaticData } from "../../../organisms/Calculator/StaticValues"


type InitString = string | null;
type InitBoolean = boolean | null;

interface IValueObj<T> {
  value: T;
}

interface IError {
  hasError: boolean;
  msg: string;
}

interface ICalculatorState {
	equation : string;
  result: string,
  inputGrid: ICalcGridStaticData,
  isCalculated: InitBoolean
  errorState: IError | null;
}

interface IUserInput extends IValueObj<string>{};

interface IEquation extends IValueObj<string>{};

interface IEquationPayload extends PayloadAction<IUserInput>{};
interface IResultPayload extends PayloadAction<IUserInput>{};
interface IErrorPayload extends PayloadAction<IError>{};


export type { ICalculatorState, IValueObj, IEquation, IEquationPayload, IResultPayload, IErrorPayload };
