interface StyleClosetTheme {
  breakpoints: { [key in keyof typeof breakpoints]: string };
  colors: { [key in keyof typeof colors]: string };
  spacing: { [key in keyof typeof spacing]: string };
  widths: { [key in keyof typeof widths]: string };
  methods: {
    setWidth: typeof setWidth;
  };
}

const colors = {
  body_background: '#f9f9f9',
  primary_dark: "#2b2725",
  primary_green: "#79D162"
};

const spacing = {
  lg: "3.5rem",
  lg_lg: "4.5",
  lg_md: "4rem",
  md: "2rem",
  md_lg: "3rem",
  md_md: "2.5rem",
  sm: ".5rem",
  sm_lg: "1.5rem",
  sm_md: "1rem",
  sm_sm: "0.25rem",
};

const breakpoints = {
  lg: "1200px",
  md: "768px",
  sm: "480px",
};

const setWidth = (percent: string, value: string): string => {
  return `calc(${percent} - ${value})`;
};

const widths = {
  containerWidth: setWidth("100%", "55px"),
};


const theme: StyleClosetTheme = {
  breakpoints,
  colors,
  methods: {
    setWidth,
  },
  spacing,
  widths,
};
export type { StyleClosetTheme };
export { theme };
