import { createGlobalStyle, ThemeProvider } from "./styled-components";
import { theme } from "./theme";

const { colors: { body_background }} = theme;

const GlobalStyle = createGlobalStyle` 

  body {
    margin: 0;
    padding: 0;
    font-family: 'Saira Condensed', sans-serif;
    background-color: ${ body_background };
  }

  h1,h2,h3,h4,h5,h6 {
    margin: 0;
    padding:0
  }
`;
export { GlobalStyle, ThemeProvider, theme };
