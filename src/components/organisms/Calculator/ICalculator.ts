import { IValueObj } from "../../../components/store/slices/CalculatorSlice/ICalculatorSlice"

interface ICalculator {}
interface IDisplayCalc extends IValueObj<string> {}
interface IDisplayResults extends IValueObj<string> {}

export type { ICalculator, IDisplayCalc, IDisplayResults };
