import React from 'react';
import { Main } from "../components"
import { GlobalStyle } from "../components"
import { Provider } from "react-redux"
import { store } from "../components/store"

const App = () => (
  <>
    <GlobalStyle />
    <Provider store={store}>
      <Main />  
    </Provider>
  </>
);


export default App;
 